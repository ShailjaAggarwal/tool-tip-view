import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
  Dimensions
} from 'react-native';
import {connect} from 'react-redux';

var WIDTH= Dimensions.get('window').width;
var HEIGHT = Dimensions.get('window').height;

const mapStateToProps = (state) => {
  return {

  }
}


const mapDispatchToProps = (dispatch) => {
  return {

  }
}


class ToolTip extends React.Component {

  static defaultProps={
    ToolTipStyle:{
            backgroundColor: '#ff3428',
            height:70,
            width:WIDTH-80,
            position: 'absolute',
            justifyContent:'center',
            alignItems:'center',
            flex: 1,
            right: 3,
            bottom:WIDTH/6,
            borderRadius:7,
          },
    toolTipTextStyle:{
      color: 'white',
      fontSize:16,
      fontFamily:(Platform.OS == 'ios'?'PingFang SC':'Roboto'),
    },
    toolTipText:'',
    tipColor:'#ff3428',
    toolTipBottomTriangle:{
                  width: 0,
                   height: 0,
                   backgroundColor: 'transparent',
                   borderStyle: 'solid',
                   overflow: 'hidden',
                   borderTopWidth: 10,
                   borderRightWidth: 8,
                   borderBottomWidth: 5,
                   borderLeftWidth: 8,
                   borderTopColor: '#ff3428',
                   borderRightColor: 'transparent',
                   borderBottomColor: 'transparent',
                   borderLeftColor: 'transparent',
                   position:'absolute',
                   bottom:-15,
                   left:30,
                 }
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentWillMount(){
  }

  render(){
    let {
      ToolTipStyle,
      toolTipTextStyle,
      toolTipText,
      tipColor,
      toolTipBottomTriangle
    }=this.props;
return(
  <View style={ToolTipStyle}>
        <Text style={toolTipTextStyle}>{toolTipText}
      </Text>
      <View style={toolTipBottomTriangle}>
        </View>
    </View>

)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToolTip)
